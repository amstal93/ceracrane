<!-- PROJECT LOGO -->

  <h3 align="center">Ceracrane</h3>
  <p align="center">
    Container measurement signature-system.
    <br />
    <a href="https://gitlab.com/kurkimiehet/ceracrane/-/wikis/Project-motivation"><strong>Find more information about the project and it's motivation by reading the Wikis! »</strong></a>
  </p>
<p align="center">
    <!--<img src="https://gitlab.com/kurkimiehet/ceracrane/-/wikis/uploads/53994088b712327ad246812eeb96c161/image.png" alt="Logo" width="350" height="330">
    <img src="https://gitlab.com/kurkimiehet/ceracrane/-/wikis/uploads/9dac545968c20b748314b4753cbc1a8c/image.png" alt="Logo" width="350" height="330">-->
    <a style="color: #FFFFFF; text-decoration: none;" href="https://gitlab.com/kurkimiehet/ceracrane/-/wikis/uploads/babbed6836484996d88a9d1150a8d66b/example-measurement.mp4" target="_blank" >
         <br />
         <img src="https://gitlab.com/kurkimiehet/ceracrane/-/wikis/uploads/4b9f381a3c5e29e40219832554fd67d7/example-measurement.gif">
    </a>
</p>


<!-- ABOUT THE PROJECT -->
## About The Project

Ship container verification and signature system for Konecranes and Aalto University. The backend-environment has been built with Docker-compose consisting of many different services. The main developing principle for the project has been portability, scalability and error isolation. We have achieved this by seggregating and abstracting said endpoint/services into REST APIs.

For information about why, how and for who, please read the [Project motivation](https://gitlab.com/kurkimiehet/ceracrane/-/wikis/Project-motivation) in the wikis.

#### Technologies used

* [IOTA & Blockhain](https://www.iota.org/)
* [Docker-compose](https://www.docker.com/)
* [Flutter](https://flutter.dev/)
* [OPC-UA](https://opcfoundation.org/about/opc-technologies/opc-ua/) Crane API

<!-- GETTING STARTED -->
## Getting Started

Since the whole environment is very large we decided to make some of the heavier Docker images available to the repository's [registry](https://gitlab.com/kurkimiehet/ceracrane/container_registry).

1. First make sure you are logged in to the GitLab registry:
```
docker login registry.gitlab.com
```
2. Bring the whole stack up:
```
docker-compose up
```

If you want to further develop or take a closer look at the microservices you can find the additional relevant install instructions inside of the corresponding to the service.

<!-- USAGE EXAMPLES -->
### Front end

Front end made using Flutter. See [README.md](./ui) at ui-directory for development instructions and  more in-depth thoughts about UI.

Even though the Flutter application is the main and easiest way to interact with the whole system you could still use for example the eIDAS-Service independently from this project. More information can be found in the eIDAS [README.md](./eidas-dss).

<!-- ROADMAP -->
## Roadmap

Currently the project has no future roadmap, but if you have any questions of enhancements to suggest please create an [issue](https://gitlab.com/kurkimiehet/ceracrane/-/issues).


<!-- CONTACT -->
## Contact

[Jaan Taponen](https://gitlab.com/janetsqy)

Henri Tunkkari

[Santeri Kääriäinen](https://gitlab.com/napuu)

...


<!-- ACKNOWLEDGEMENTS -->
## Acknowledgements

* [EIDAS](https://github.com/esig/dss)
