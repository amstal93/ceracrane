import 'dart:async';
import 'dart:convert';
import 'dart:core';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:eventify/eventify.dart';
import 'StyleUtils.dart';

import 'Constants.dart' as Constants;
import 'ConnectionStatus.dart';
import 'MeasurementWaterfall.dart';
import 'package:fluttertoast/fluttertoast.dart';

class OperatorView extends StatefulWidget {
  @override
  _OperatorViewState createState() => _OperatorViewState();
}

class _OperatorViewState extends State<OperatorView> {
  void handlePressed() async {
    if (_isWaitingForBackend) {
      print("button already pressed");
      return;
    }
    if (_containerID.length == 0) {
      print("insert container id");
      Fluttertoast.showToast(
          msg: "Container ID is needed!",
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 3,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
      return;
    }
    try {
      print("creating job...");
      setState(() {
        _waterfallRunning = true;
        _isWaitingForBackend = true;
      });
      http.Response resp = await http
          .get(Constants.opcRestAddress + '/createjob?id=' + _containerID);
      emitter.emit("job", 0, 0);
      setState(() {
        _jobRunning = true;
        _jobId = json.decode(resp.body)['jobid'];
      });
    } catch (err) {
      print("creation of the job failed" + err);
    }
  }

  void resetState(String jobStatusText) {
    setState(() {
      _jobRunning = false;
      _isWaitingForBackend = false;
    });
  }

  void fetchJobStatus() async {
    if (!mounted) return;
    try {
      http.Response resp =
          await http.get(Constants.opcRestAddress + '/status?id=' + _jobId);
      Map<String, dynamic> jobStatusResponse = json.decode(resp.body);

      int jobStatus = jobStatusResponse['statusnum'];
      emitter.emit("job", 0, jobStatus);
      setState(() {
        _jobStatus = jobStatus;
      });
      String plainText = "";
      if (Constants.JOB_STATUS.containsKey(jobStatus)) {
        plainText = Constants.JOB_STATUS[jobStatus];
      }
      if (jobStatus == -1) {
        print("failed");
        setState(() {
          _jobStatus = -1;
          _jobRunning = false;
        });
        resetState(plainText);
        emitter.emit("reset");
        return;
      } else if (jobStatus == 5) {
        resetState(plainText);
        return;
      }
      bool keepPolling = true;
      if (jobStatus == 5) {
        keepPolling = false;
      }
      setState(() {
        _jobStatus = jobStatus;
        _jobRunning = keepPolling;
      });
    } catch (err) {
      setState(() {
        _craneConnectionOK = false;
      });
      emitter.emit("reset");
      print("COULD NOT CONNECT TO CRANE");
      print(err);
    }
  }

  void fetchStatusFromCraneToState() async {
    if (!mounted) return;
    if (!_jobRunning && !_waterfallRunning) {
      try {
        http.Response resp =
            await http.get(Constants.opcRestAddress + '/cranestatus');
        Map<String, dynamic> measurement = json.decode(resp.body);
        DateTime datetime = DateTime.parse(measurement['datetime']);
        String padLeft(int number) {
          if (number < 10) return "0" + number.toString();
          return number.toString();
        }

        String year = padLeft(datetime.year);
        String month = padLeft(datetime.month);
        String day = padLeft(datetime.day);

        String hours = padLeft(datetime.hour);
        String minutes = padLeft(datetime.minute);
        String seconds = padLeft(datetime.second);

        setState(() {
          _craneConnectionOK = true;
          _weight = measurement['weight'].toStringAsFixed(3) + " Tkg";
          _taredWeight = measurement['tared'].toStringAsFixed(3) + " Tkg";
          _dateTime = year +
              "/" +
              month +
              "/" +
              day +
              " " +
              hours +
              ":" +
              minutes +
              ":" +
              seconds +
              " UTC+0";
        });
      } catch (err) {
        setState(() {
          _craneConnectionOK = false;
        });
        print("COULD NOT FETCH CRANESTATUS");
        print(err);
      }
    }
  }

  EventEmitter emitter = new EventEmitter();
  void initState() {
    super.initState();

    emitter.on("waterfalldone", this, (ev, context) {
      setState(() {
        _waterfallRunning = false;
      });
    });

    void load() {
      if (!mounted) return;
      setState(() {
        _load += 0.03;
      });
      if (_load < 1)
        Timer(Duration(milliseconds: 25), () {
          load();
        });
    }

    Timer(Duration(milliseconds: 200), load);

    _timer = Timer.periodic(new Duration(seconds: 1), (timer) {
      fetchStatusFromCraneToState();
      if (_jobRunning) fetchJobStatus();
    });
  }

  double _load = 0;
  String _weight = "-1";
  String _taredWeight = "123";
  String _containerID = "";
  String _dateTime = "2013-02-02";
  String _jobId = "initialID";
  int _jobStatus = -2;
  bool _craneConnectionOK;

  bool _isWaitingForBackend = false;
  bool _jobRunning = false;
  bool _waterfallRunning = false;
  Timer _timer;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Stack(
        children: <Widget>[
          AnimatedOpacity(
            opacity: (_load < 1 ? 1 : 0),
            duration: Duration(milliseconds: 100),
            child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 200, vertical: 200),
                child: LinearProgressIndicator(
                  value: _load,
                )),
          ),
          AnimatedOpacity(
              opacity: (_load < 1 ? 0 : 1),
              duration: Duration(milliseconds: 300),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(top: 50),
                          width: 600,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget> [
                              Container(
                                //alignment: Alignment.centerRight,
                                height: 150,
                                width: 250,
                                decoration: BoxDecoration(
                                  image: DecorationImage(image: AssetImage("res/logo3.png")),
                                )
                              ),
                            ],
                          ),
                        ),
                        Padding(
                            /*[>padding: const EdgeInsets.fromLTRB(40, 100, 0, 0),<]*/
                            padding: const EdgeInsets.fromLTRB(0, 30, 0, 30),
                            child: Text(
                                'Insert container ID to create measurement',
                                style: GoogleFonts.roboto(
                                  textStyle:
                                      Theme.of(context).textTheme.headline1,
                                  fontSize: 30,
                                  fontWeight: FontWeight.w300,
                                ))),
                        Row(
                          key: Key("statusRow"),
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: 300,
                              /*decoration: BoxDecoration(*/
                              /*border: Border.all(color: Colors.blueAccent)),*/
                              child: Column(
                                key: Key("craneStatusColumn"),
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text("Crane status",
                                      style: headline(context)),
                                  Row(children: [
                                    Text("Connection status: ",
                                        style: body(context)),
                                    ConnectionStatus(
                                        statusOK: _craneConnectionOK)
                                  ]),
                                  Row(children: [
                                    Text("Name: ", style: body(context)),
                                    Text(Constants.craneName,
                                        style: bodyBold(context))
                                  ]),
                                  Row(children: [
                                    Text("Date: ", style: body(context)),
                                    Text(_dateTime, style: bodyBold(context))
                                  ]),
                                ],
                              ),
                            ),
                            Container(
                                width: 300,
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Text("Container info",
                                                style: headline(context)),
                                            Row(children: <Widget>[
                                              Column(
                                                key: Key(
                                                    "containerStatusColumn"),
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  Text("Weight: ",
                                                      style: body(context)),
                                                  Text("Tared weight: ",
                                                      style: body(context)),
                                                  Text("Container id: ",
                                                      style: body(context)),
                                                ],
                                              ),
                                              Column(
                                                key: Key(
                                                    "containerStatusColumnValues"),
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.end,
                                                children: <Widget>[
                                                  Text(_weight,
                                                      style: bodyBold(context)),
                                                  Text(_taredWeight,
                                                      style: bodyBold(context)),
                                                  Container(
                                                    padding:
                                                        EdgeInsets.fromLTRB(
                                                            0, 10, 0, 0),
                                                    width: 150.0,
                                                    height: 30.0,
                                                    child: TextField(
                                                      inputFormatters: [
                                                        LengthLimitingTextInputFormatter(14),
                                                      ],
                                                      onChanged:
                                                          (String value) {
                                                        _containerID = value;
                                                      },
                                                      textAlign: TextAlign.end,
                                                      style: bodyBold(context),
                                                    ),
                                                  ),
                                                ],
                                              )
                                            ])
                                          ])
                                    ]))
                          ],
                        ),
                        Padding(
                          padding: EdgeInsets.fromLTRB(0, 30, 0, 10),
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text("Current measurement",
                                        style: headline(context)),
                                    AnimatedContainer(
                                      child: Waterfall(emitter),
                                      duration: Duration(milliseconds: 300),
                                      height: 150,
                                      width: 230
                                    ),
                                  ],
                                ),
                                Column(
                                  children: <Widget>[
                                    AnimatedOpacity(
                                        duration: Duration(milliseconds: 500),
                                        opacity: (_waterfallRunning ? 0 : 1),
                                        child: Padding(
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 80, vertical: 10),
                                            child: RaisedButton(
                                                onPressed: _jobRunning ||
                                                        _waterfallRunning ||
                                                        _load < 1 ||
                                                        !_craneConnectionOK
                                                    ? null
                                                    : () {
                                                        handlePressed();
                                                      },
                                                child: Text(
                                                    "Create a measurement",
                                                    style: TextStyle(
                                                        fontSize: 20))))),
                                    AnimatedOpacity(
                                        duration: Duration(milliseconds: 500),
                                        opacity: (_waterfallRunning ? 1 : 0),
                                        child: Padding(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 80),
                                          child: Container(
                                              width: 50,
                                              height: 50,
                                              child:
                                                  CircularProgressIndicator()),
                                        )),
                                  ],
                                )
                              ]),
                        ),
                      ])
                ],
              ))
        ],
      )),
    );
  }
}
