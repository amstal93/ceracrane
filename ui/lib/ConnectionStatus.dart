import 'dart:core';

import 'package:flutter/material.dart';

class ConnectionStatus extends StatelessWidget {
  final bool statusOK;
  ConnectionStatus({this.statusOK});
  @override
  Widget build(BuildContext context) {
    if (statusOK == true) {
      return Text(
        "OK",
        style: TextStyle(
          fontSize: 30,
          color: Colors.lightGreen,
        ),
      );
    } else if (statusOK == false) {
      return Text(
        "NOT OK",
        style: TextStyle(
          fontSize: 30,
          color: Color(0xffa31621),
        ),
      );
    } else {
      return Text("",
        style: TextStyle(
          fontSize: 30,
          color: Color(0xffa31621),
        ));
    }
  }
}
