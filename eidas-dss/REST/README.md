# Running the webapp with Docker:
```console
docker build -t signer . && docker run -it -d -p 8080:8080 signer
```

# Or alternatively get the image straight from the registry:
```console
docker login registry.gitlab.com
docker run -it -p 8080:8080 registry.gitlab.com/kurkimiehet/ceracrane/eidas-rest
```
The HTML page is available via ```http://localhost:8080/```

**Documentation on this service can be found [here](https://github.com/AaltoSmartCom/dss-demonstrations/tree/master/documentation).**