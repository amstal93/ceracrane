const express = require('express');
const app = express();
const fs = require('fs');
const path = require('path');
const execSync = require('child_process').execFileSync;
const exec = require('child_process').exec;
const bodyParser = require('body-parser');

const port = 3000;

app.get('/ping', (req, res) => res.send('pong'));
app.use(bodyParser.json({'limit': '50mb'}));

app.post('/sign', (req, res) => {
  const keystoreFile = "temp/keystore.pks";
  const documentFile = "temp/document.xml";
  const keystoreKey = req.body.keystore.key;
  fs.writeFile(keystoreFile, req.body.keystore.bytes, 'base64', function(err) {
    if (err) console.log(err);
  });
  fs.writeFile(documentFile, req.body.toSignDocument.bytes, 'base64', function(err) {
    if (err) console.log(err);
  });

  const gradleString = `/bin/bash -c "gradle run --args='${documentFile} ${keystoreFile} ${keystoreKey}'"`;
  console.log(gradleString);
  exec(gradleString, (err, stdout, stderr) => {
    console.log(stdout) 
    if (err) {
      console.log(stderr);
    }
    res.sendFile("/home/signeDB.xml");
    removeFiles([keystoreFile, documentFile]);
  });
});

const removeFiles = (arr) => {
  arr.forEach(filu => {
    exec(`/bin/bash -c "rm ${filu}"`, (err, stdout, stderr) => {
      console.log(stdout);
      if (err) {
        console.error(stderr);
      }
    });
  });
};

app.listen(port, () => console.log(`app listening on port ${port}!`));
