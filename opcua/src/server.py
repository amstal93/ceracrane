import sys
sys.path.insert(0, "..")
import time
import random
import datetime
from opcua import ua, Server
sys.path.insert(0, "..")
from pathlib import Path
class Mockup:
    def __init__(self):
        # setup our server
        self.server = Server()
        self.server.set_endpoint("opc.tcp://0.0.0.0:4840/freeopcua/server/")
        uri = "SYM:"
        idx = self.server.register_namespace(uri)
        self.server.import_xml(str(Path(__file__).resolve().parent) + '/xml/custom_nodes.xml')
        self.date = datetime.datetime.now()
        
    def updatValues(self):
        self.date = datetime.datetime.now()
        NS = "ns=" + str(self.server.get_namespace_index("SYM:"))
        self.server.get_node(
            NS + ";s=SCF.PLC.DX_Custom_V.Datetime.Year").set_value((self.date.year))
        self.server.get_node(
            NS + ";s=SCF.PLC.DX_Custom_V.Datetime.Month").set_value((self.date.month))
        self.server.get_node(
            NS + ";s=SCF.PLC.DX_Custom_V.Datetime.Day").set_value((self.date.day))
        self.server.get_node(
            NS + ";s=SCF.PLC.DX_Custom_V.Datetime.Hour").set_value((self.date.hour))
        self.server.get_node(
            NS + ";s=SCF.PLC.DX_Custom_V.Datetime.Minute").set_value((self.date.minute))
        self.server.get_node(
            NS + ";s=SCF.PLC.DX_Custom_V.Datetime.Second").set_value((self.date.second))
        #self.server.get_node(
        #    NS + ";s=SCF.PLC.DX_Custom_V.Datetime.Millisecond").set_value((self.date.microsecond*1000))
        
        r = 5 + random.random() - 0.5
        self.server.get_node(NS + ";s=SCF.PLC.DX_Custom_V.Status.Hoist.Load.Load_t").set_value(r)
        self.server.get_node(NS + ";s=SCF.PLC.DX_Custom_V.Status.Hoist.Load.TaredLoad_t").set_value(r)
        
if __name__ == "__main__":
    try:
        mockup = Mockup()
        mockup.server.start()
        count = 0
        while True:
            time.sleep(1)
            mockup.updatValues()
    finally:
        mockup.server.stop()
